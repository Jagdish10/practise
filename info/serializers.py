from django.contrib.auth.models import User,Group
from rest_framework import serializers
from info.models import Snippet,LANGUAGE_CHOICES,STYLE_CHOICES
class UserSerializer(serializers.HyperlinkedModelSerializer):
    snippets=serializers.HyperlinkedRelatedField(many=True,view_name='snippet-detail',read_only=True)
    class Meta:
        model=User
        fields=('url','id','username','snippets')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta: 
        model=Group
        fields=('urls','name')

# class SnippetSerializer(serializers.Serializer):
#     id=serializers.IntegerField(read_only=True)
#     title = serializers.CharField(required=False, allow_blank=True, max_length=100)
#     code = serializers.CharField(style={'base_template': 'textarea.html'})
#     linenos = serializers.BooleanField(required=False)
#     language = serializers.ChoiceField(choices=LANGUAGE_CHOICES, default='python')
#     style = serializers.ChoiceField(choices=STYLE_CHOICES, default='friendly')

#     def create(self,validated_date):
#         return Snippet.objects.create(**validated_date)


#     def update(self,instance,validated_date):
#         instance.title=validated_date.get('title',instance.title)
#         instance.code=validated_date.get('code',instance.code)
#         instance.lineos=validated_date.get('lineos',instance.linenos)
#         instance.language=validated_date.get('language',instance.language)
#         instance.style=validated_date.get('style',instance.style)
#         instance.save()
#         return instance

class SnippetSerializer(serializers.HyperlinkedModelSerializer):
    owner=serializers.ReadOnlyField(source='owner.username')
    highlight=serializers.HyperlinkedIdentityField(view_name='snippet-highlight',format='html')
    class Meta:
        model = Snippet
        fields = ('url','id','highlight', 'title', 'code', 'linenos', 'language', 'style','owner')
        